# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OperationsToCustomersMapper do
  let(:operations) do
    [
      Operation.new(by: 'A', type: 'recommends', target: 'B', timestamp: DateTime.parse('2018-06-12 09:41')),
      Operation.new(by: 'B', type: 'accepts', timestamp: DateTime.parse('2018-06-14 09:41')),
      Operation.new(by: 'B', type: 'recommends', target: 'C', timestamp: DateTime.parse('2018-06-16 09:41')),
      Operation.new(by: 'C', type: 'accepts', timestamp: DateTime.parse('2018-06-17 09:41')),
      Operation.new(by: 'C', type: 'recommends', target: 'D', timestamp: DateTime.parse('2018-06-19 09:41')),
      Operation.new(by: 'B', type: 'recommends', target: 'D', timestamp: DateTime.parse('2018-06-23 09:41')),
      Operation.new(by: 'D', type: 'accepts', timestamp: DateTime.parse('2018-06-25 09:41')),
      Operation.new(by: 'E', type: 'accepts', timestamp: DateTime.parse('2018-06-26 09:41'))
    ]
  end

  describe 'maps customers with correct' do
    subject { OperationsToCustomersMapper.new(operations).run }

    it 'count as per unique names' do
      expect(subject.count).to eq(5)
    end

    it 'keys as per unique names' do
      expect(subject.keys).to eq(%w[A B C D E])
    end

    it 'objects against each key' do
      subject.each { |key, customer| expect(key).to eq(customer.name) }
    end

    it 'reference_by mappings' do
      expect(subject['A'].referred_by).to be(nil)
      expect(subject['B'].referred_by).to be(subject['A'])
      expect(subject['C'].referred_by).to be(subject['B'])
      expect(subject['D'].referred_by).to be(subject['C'])
      expect(subject['E'].referred_by).to be(nil)
    end

    it 'referred_at mappings' do
      expect(subject['A'].referred_at).to be(nil)
      expect(subject['B'].referred_at).to eq(DateTime.parse('2018-06-12 09:41'))
      expect(subject['C'].referred_at).to eq(DateTime.parse('2018-06-16 09:41'))
      expect(subject['D'].referred_at).to eq(DateTime.parse('2018-06-19 09:41'))
      expect(subject['E'].referred_at).to be(nil)
    end

    it 'referrals mappings' do
      expect(subject['A'].referrals).to eq([subject['B']])
      expect(subject['B'].referrals).to eq([subject['C']])
      expect(subject['C'].referrals).to eq([subject['D']])
      expect(subject['D'].referrals).to eq([])
      expect(subject['E'].referrals).to eq([])
    end
  end
end
