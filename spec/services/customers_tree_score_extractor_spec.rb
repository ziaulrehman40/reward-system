# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CustomersTreeScoreExtractor do
  let(:customer_a) { Customer.new('A') }
  let(:customer_b) { Customer.new('B') }
  let(:customer_c) { Customer.new('C') }
  let(:customer_d) { Customer.new('D') }

  before(:each) do
    customer_a.add_referral(customer_b)
    customer_b.add_referral(customer_c)
    customer_c.add_referral(customer_d)
  end

  it 'maps scores of all customers in tree against their names' do
    scores = CustomersTreeScoreExtractor.new(customer_a).run
    expect(scores).to eq('A' => 1.75, 'B' => 1.5, 'C' => 1, 'D' => 0)
  end
end
