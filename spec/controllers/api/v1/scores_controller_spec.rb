# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::ScoresController, type: :controller do
  let(:params) do
    {
      file: fixture_file_upload(Rails.root.join('spec', 'files', 'mlm.txt'), 'text/plain')
    }
  end

  let(:malformed_params) do
    {
      file: fixture_file_upload(Rails.root.join('spec', 'files', 'mlm_malformed.txt'), 'text/plain')
    }
  end

  describe '#calculate_scores' do
    it 'processes operations file returning scores calculated' do
      post :calculate_scores, params: params
      scores = JSON.parse(response.body)
      expect(scores).to eq('A' => 1.75, 'B' => 1.5, 'C' => 1)
      expect(response.code).to eq('200')
    end

    it 'gracefully handles malformed file' do
      post :calculate_scores, params: malformed_params
      resp = JSON.parse(response.body)
      expect(resp['error']).to eq('Invalid file')
      expect(response.code).to eq('400')
    end
  end
end
