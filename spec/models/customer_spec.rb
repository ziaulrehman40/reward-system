# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Customer do
  let(:obj1) { Customer.new('A') }
  let(:obj2) { Customer.new('B') }

  it 'initializes correctly' do
    expect(obj1.name).to eq('A')
  end

  it 'saves and returns its attributes correctly' do
    %w[name score referred_by referred_at referrals].each do |attrib|
      val = "#{attrib}_value"
      obj1.send("#{attrib}=", val)
      expect(obj1.send(attrib)).to eq(val)
    end
  end

  describe '#set_reference' do
    it 'sets reference properly' do
      dt = DateTime.now
      obj1.set_reference(obj2, dt)
      expect(obj1.referred_by).to be(obj2)
      expect(obj1.referred_at).to be(dt)
    end
  end

  describe '#accept_reference' do
    it 'sets reference properly' do
      obj1.accept_reference
      expect(obj1.invitation_accepted).to be(true)
    end
  end

  describe '#add_referral' do
    it 'sets reference properly' do
      obj1.add_referral(obj2)
      expect(obj1.referrals).to eq([obj2])
    end
  end
end
