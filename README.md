# Reward System

**A system for calculating rewards based on recommendations of customers.**

### Assumptions
1- Records **might not be sorted** by date. Assumption taken to cover possible edge case.

2- **No DB persistence** is required, just file processing is needed returning results JSON in response. Assumption taken because no DB or persistence mention is present in the question.

3- Assumption number 2 also implies this API end-point cannot process very huge files, that can cause timeouts as huge files processing needs background jobs etc, so cannot be achieved along with the required behavior.

4- There can be multiple **people joining on their own**, without being invited by anyone(although example has only 1 such person).

5- **Input file is a `txt` file**, not a proper `csv`. This is assumed as no headers were given in the example.

6- Example output suggests only those customers should be returned which got **score > 0**.

7- Customer **names cannot contain spaces**. This assumption is taken to avoid un-necessary complexity in parsing, as input data had no such record either.

8- There can be **invitation acceptances without being invited**.*(this is kind of malformed data, but we will parse it anyway assuming it is ok and should be treated as someone joining on their own).*

### Algorithm
*The problem was more like ones we used to do in speed programming competitions, no DB, just an input format and required output format.*

As DB persistence was not required, this effected a lot of design decisions. Basic algorithm in a nutshell is to **map customers in a tree structure based on the valid accepted referrals** and than run a **Depth First Search** algorithm to iterate tree in an optimized manner.

While iterating with DFS i have mapped the score calculation formula to be:
`parents_score = number_of_childs + (sum_of_score_of_all_childs / 2)`

This allows us to avoid the callback/chaining costs on each referral and we are able to calculate scores in one go.

### How to run?
Requirements:
```
Ruby: 2.6.3
Bundler: 2.x
```

1- Clone

2- Run `bundle install` will install all gem dependencies.

3- Start server by running `rails s` or `bundle exec rails s`.

4- Now you can use your favorite method to make a post call to `localhost:3000/api/v1/scores/calculate_scores` with a file attachment under `file` parameter name. 

**OR**

you can use simple curl command from project root directory: `curl -F file=@spec/files/mlm.txt http://localhost:3000/api/v1/scores/calculate_scores` this uses already present example input. If you want to use your own file, you can do so by replacing `spec/files/mlm.txt` part which is file path.

5- To run specs, from project root run `rspec`.
