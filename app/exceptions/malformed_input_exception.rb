# frozen_string_literal: true

class MalformedInputException < RuntimeError
end
