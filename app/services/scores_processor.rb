# frozen_string_literal: true

class ScoresProcessor
  attr_accessor :parsed_operations, :customers_mapper, :customer_tree_score_extractor, :keep_zero_values

  def initialize(raw_operations, keep_zero_values = false,
                 raw_operations_parser: 'RawOperationsParser',
                 customers_mapper: 'OperationsToCustomersMapper',
                 customer_tree_score_extractor: 'CustomersTreeScoreExtractor')
    @parsed_operations = raw_operations_parser.constantize.new(raw_operations).run
    @parsed_operations.sort_by!(&:timestamp)

    @customers_mapper = customers_mapper
    @customer_tree_score_extractor = customer_tree_score_extractor
    @keep_zero_values = keep_zero_values
  end

  def run
    scores = {}

    customers_map   = customers_mapper.constantize.new(parsed_operations).run
    customers_trees = customers_map.values.reject(&:invitation_accepted)

    customers_trees.each do |tree|
      scores.merge!(customer_tree_score_extractor.constantize.new(tree).run)
    end

    scores.reject! { |_name, score| score.zero? } unless keep_zero_values
    scores
  end
end
