# frozen_string_literal: true

class CustomersTreeScoreExtractor
  attr_accessor :customers_tree, :scores

  def initialize(customers_tree)
    @customers_tree = customers_tree
    @scores = {}
  end

  def run
    calculate_node_scores(customers_tree)
    scores
  end

  private

  def calculate_node_scores(node)
    referrals_scores_sum = 0

    node.referrals.each do |ref|
      calculate_node_scores(ref)
      referrals_scores_sum += ref.score
    end

    node.score = node.referrals.count + (referrals_scores_sum / 2.0)
    scores[node.name] = node.score
  end
end
