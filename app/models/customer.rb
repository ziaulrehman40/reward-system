# frozen_string_literal: true

class Customer
  attr_accessor :name, :score, :referred_by, :referred_at, :invitation_accepted, :referrals

  def initialize(name)
    @name = name
    @referrals = []
  end

  def set_reference(referrer, referred_at)
    self.referred_by = referrer
    self.referred_at = referred_at
  end

  def accept_reference
    self.invitation_accepted = true
  end

  def add_referral(referral)
    referrals << referral
  end
end
