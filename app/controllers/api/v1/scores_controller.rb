# frozen_string_literal: true

module Api
  module V1
    class ScoresController < ApplicationController
      def calculate_scores
        file_path = input_file.path
        scores = ScoresProcessor.new(File.foreach(file_path)).run
        render json: scores.to_json
      rescue MalformedInputException, Errno::ENOENT, ActionController::ParameterMissing
        render json: { error: 'Invalid file' }, status: :bad_request
      end

      private

      def input_file
        params.require(:file)
      end
    end
  end
end
