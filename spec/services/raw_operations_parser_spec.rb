# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RawOperationsParser do
  let(:raw_operations) do
    [
      '2018-06-12 09:41 A recommends B',
      '2018-06-14 09:41 B accepts'
    ]
  end

  let(:invalid_date_operation) { 'invalid date A recommends B' }
  let(:too_few_args_operation1) { '2018-06-12 09:41 A recommends' }
  let(:too_few_args_operation2) { '2018-06-12 09:41 B' }
  let(:too_many_args_operation1) { '2018-06-12 09:41 A recommends B and C' }
  let(:too_many_args_operation2) { '2018-06-12 09:41 A accepts offer by B' }

  it 'parses valid operations' do
    parsed_ops = RawOperationsParser.new(raw_operations).run
    expect(parsed_ops.count).to eq(raw_operations.count)

    expect(parsed_ops[0].by).to eq('A')
    expect(parsed_ops[0].type).to eq('recommends')
    expect(parsed_ops[0].timestamp).to eq(DateTime.parse('2018-06-12 09:41'))
    expect(parsed_ops[0].target).to eq('B')

    expect(parsed_ops[1].by).to eq('B')
    expect(parsed_ops[1].type).to eq('accepts')
    expect(parsed_ops[1].timestamp).to eq(DateTime.parse('2018-06-14 09:41'))
    expect(parsed_ops[1].target).to eq(nil)
  end

  describe 'invalid input raises MalformedInputException when' do
    it 'date parsing fails' do
      expect { RawOperationsParser.new([invalid_date_operation]).run }.to raise_error(MalformedInputException)
    end

    describe 'wrong number of arguments, ' do
      it 'too few' do
        expect { RawOperationsParser.new([too_few_args_operation1]).run }.to raise_error(MalformedInputException)
        expect { RawOperationsParser.new([too_few_args_operation2]).run }.to raise_error(MalformedInputException)
      end

      it 'too many' do
        expect { RawOperationsParser.new([too_many_args_operation1]).run }.to raise_error(MalformedInputException)
        expect { RawOperationsParser.new([too_many_args_operation2]).run }.to raise_error(MalformedInputException)
      end
    end
  end
end
