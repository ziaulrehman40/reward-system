# frozen_string_literal: true

class OperationsToCustomersMapper
  attr_accessor :operations, :node_type, :trees_roots, :customers_map

  def initialize(operations, node_type = 'Customer')
    @operations  = operations
    @node_type   = node_type
    @trees_roots = []
  end

  def run
    self.customers_map = {}

    operations.each do |operation|
      if operation.type == 'recommends'
        parse_recommendation(operation)
      else
        parse_acceptance(operation)
      end
    end

    customers_map
  end

  private

  def parse_recommendation(operation)
    timestamp = operation.timestamp
    referrer  = customer_object(operation.by)
    referral  = customer_object(operation.target)

    referral.set_reference(referrer, timestamp) unless referral.referred_by
  end

  def parse_acceptance(operation)
    referral = customer_object(operation.by)
    referrer = customers_map[referral.referred_by&.name]

    return unless referrer

    referral.accept_reference
    customers_map[referral.referred_by.name].add_referral(referral)
  end

  def customer_object(name)
    customers_map[name] ||= node_type.constantize.new(name)
  end
end
