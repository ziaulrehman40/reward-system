# frozen_string_literal: true

class Operation
  attr_accessor :by, :type, :target, :timestamp

  def initialize(by: nil, type: nil, target: nil, timestamp: nil)
    @by = by
    @type = type
    @target = target
    @timestamp = timestamp
  end
end
