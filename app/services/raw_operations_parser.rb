# frozen_string_literal: true

class RawOperationsParser
  attr_accessor :raw_operations

  def initialize(raw_operations)
    @raw_operations = raw_operations
  end

  def run
    raw_operations.map { |raw_operation| parse_operation(raw_operation) }
  end

  private

  def parse_operation(raw_operation)
    parts = raw_operation.split(' ')
    raise MalformedInputException unless valid_parts?(parts)

    operation = Operation.new
    operation.timestamp = DateTime.parse("#{parts[0]} #{parts[1]}")
    operation.by        = parts[2]
    operation.type      = parts[3]
    operation.target    = parts.count == 5 ? parts[4] : nil

    operation
  rescue ArgumentError
    raise MalformedInputException
  end

  def valid_parts?(parts)
    %w[recommends accepts].include?(parts[3]) &&
      ((parts[3] == 'recommends' && parts.count == 5) || (parts[3] == 'accepts' && parts.count == 4))
  end
end
