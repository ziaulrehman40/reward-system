# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ScoresProcessor do
  let(:raw_operations) do
    [
      '2018-06-12 09:41 A recommends B',
      '2018-06-14 09:41 B accepts',
      '2018-06-16 09:41 B recommends C',
      '2018-06-17 09:41 C accepts',
      '2018-06-19 09:41 C recommends D',
      '2018-06-23 09:41 B recommends D',
      '2018-06-25 09:41 D accepts'
    ]
  end

  describe 'keep_zero_values param' do
    it 'returns non_zero scores by default' do
      scores = ScoresProcessor.new(raw_operations).run
      expect(scores).to eq('A' => 1.75, 'B' => 1.5, 'C' => 1)
    end

    it 'returns all scores when keep_zero_values is true' do
      scores = ScoresProcessor.new(raw_operations, true).run
      expect(scores).to eq('A' => 1.75, 'B' => 1.5, 'C' => 1, 'D' => 0)
    end
  end
end
