# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Operation do
  describe '#initialize' do
    it 'initializes correctly with parameters' do
      dt = DateTime.now
      obj = Operation.new(by: 'A', type: 'recommends', target: 'B', timestamp: dt)
      expect(obj.by).to eq('A')
      expect(obj.type).to eq('recommends')
      expect(obj.timestamp).to eq(dt)
      expect(obj.target).to eq('B')
    end

    it 'initializes correctly with no parameters' do
      obj = Operation.new
      expect(obj.by).to eq(nil)
      expect(obj.type).to eq(nil)
      expect(obj.timestamp).to eq(nil)
      expect(obj.target).to eq(nil)
    end
  end

  it 'saves and returns its attributes correctly' do
    obj = Operation.new
    %w[by type target timestamp].each do |attrib|
      val = "#{attrib}_value"
      obj.send("#{attrib}=", val)
      expect(obj.send(attrib)).to eq(val)
    end
  end
end
